#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <istream>
#include <vector>
#include <sstream>
#include <QMainWindow>
#include <qsignalmapper.h>
#include <QSystemTrayIcon>
using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QString shell(QString shell);
    QMenu *mMenu;
    QAction *mShowMainAction;
    QAction *maddAction;
    QAction *startAction;
    QAction *stopAction;
    QAction *mExitAppAction;
private slots:
    void createActions();
    void createMenu();
    void on_pushButton_clicked();
    void on_startAction();
    void on_stopAction();
    void on_addAction();
    void on_showMainAction();
    void closeEvent( QCloseEvent * event );
    void on_exitAppAction();
    void remove(QString words);
    void edit(QString words);
    string turnstring(QString words);
    //int OnSystemTrayClicked(QSystemTrayIcon::ActivationReason reason);
    void on_activatedSysTrayIcon(QSystemTrayIcon::ActivationReason reason);

    void on_pushButton_2_pressed();

    void flash();

    void on_pushButton_3_clicked();

    void pan();

private:
    QSignalMapper *signalMapper;
    QSignalMapper *signalMapper2;
    QSystemTrayIcon *mSysTrayIcon;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
