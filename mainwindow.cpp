#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QProcess>
#include <qdebug.h>
#include <QDebug>
#include <QLabel>
#include <QPushButton>
#include <QLayout>
#include <iostream>
#include <string>
#include <istream>
#include <vector>
#include <sstream>
#include <QProcess>
#include <QSystemTrayIcon>
#include <QMessageBox>
#include <QCloseEvent>

using namespace std;
vector<string> split(const string& src, string separate_character)
{
    vector<string> strs;
    int separate_characterLen = separate_character.size();//分割字符串的长度,这样就可以支持如“,,”多字符串的分隔符
    int lastPosition = 0, index = -1;
    while (-1 != (index = src.find(separate_character, lastPosition)))
    {
        strs.push_back(src.substr(lastPosition, index - lastPosition));
        lastPosition = index + separate_characterLen;
    }
    string lastString = src.substr(lastPosition);//截取最后一个分隔符后的内容
    if (!lastString.empty())
        strs.push_back(lastString);//如果最后一个分隔符后还有内容就入队
    return strs;
}
void MainWindow::closeEvent( QCloseEvent* event )//关闭窗口会先处理该事件函数
{
  switch( QMessageBox::information( this, tr("是否退出？"),
    tr("真要退出嘛喵？"),
    tr("我是真确定了"), tr("你藏到托盘里吧"),
    0, 1 ) )
{
  case 0:
      event->accept();
      break;
  case 1:
      this->hide();
  default:
      event->ignore();
      break;
}
}
void MainWindow::createActions()
{
    mShowMainAction = new QAction(QObject::trUtf8("显示主界面"),this);
    connect(mShowMainAction,SIGNAL(triggered()),this,SLOT(on_showMainAction()));

    maddAction = new QAction(QObject::trUtf8("加一个吧"),this);
    connect(maddAction,SIGNAL(triggered()),this,SLOT(on_addAction()));

    mExitAppAction = new QAction(QObject::trUtf8("退出"),this);
    connect(mExitAppAction,SIGNAL(triggered()),this,SLOT(on_exitAppAction()));

    startAction = new QAction(QObject::trUtf8("开启程序"),this);
    connect(startAction,SIGNAL(triggered()),this,SLOT(on_startAction()));

    stopAction = new QAction(QObject::trUtf8("关闭服务"),this);
    connect(stopAction,SIGNAL(triggered()),this,SLOT(on_stopAction()));

}
void MainWindow::createMenu()
{
    mMenu = new QMenu(this);
    //新增菜单项---显示主界面
    mMenu->addAction(mShowMainAction);
    //增加分隔符
    mMenu->addSeparator();
    //新增菜单项---显示主界面
    mMenu->addAction(maddAction);
    //增加分隔符
    mMenu->addSeparator();
    //新增菜单项---开启程序
    mMenu->addAction(startAction);
    //增加分隔符
    mMenu->addSeparator();
    //新增菜单项---关闭服务
    mMenu->addAction(stopAction);
    //增加分隔符
    mMenu->addSeparator();
    //新增菜单项---退出程序
    mMenu->addAction(mExitAppAction);
    //把QMenu赋给QSystemTrayIcon对象
    mSysTrayIcon->setContextMenu(mMenu);
}
void MainWindow::on_showMainAction()
{
    this->show();
}
void MainWindow::on_addAction()
{
    MainWindow::on_pushButton_clicked();
}
void MainWindow::on_startAction()
{
    MainWindow::on_pushButton_2_pressed();
}
void MainWindow::on_stopAction()
{
    MainWindow::on_pushButton_3_clicked();
}
void MainWindow::on_exitAppAction()
{
    exit(0);
}
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    MainWindow::flash();
    ui->lineEdit->setText("bfffffff");
    MainWindow::pan();
}

MainWindow::~MainWindow()
{
    delete ui;
}

string MainWindow::turnstring(QString words)
{
    QByteArray ba = words.toLatin1();
    string first=ba.data();
    return(first);
}
QString MainWindow::shell(QString shell)
{
    FILE * fp;
    int res;
    QString  pword;
    char resultStr[100] = {"0"}; //define MAX_USB_NUM 3
    char* ch; QByteArray ba = shell.toLatin1(); ch=ba.data();
    //执行预先设定的命令，并读出该命令的标准输出
    cout<<ch<<endl;
     fp = popen(ch, "r");
     if(!fp)
     {
        perror("popen fail");
        printf("popen error: %s/n", strerror(errno));
        return "";
     }

       /* 循环读管道 #define MAX_USB_NUM 3  */
       while(fgets(resultStr, 3, fp) ){pword+=QString(QLatin1String(resultStr));}
       /*等待命令执行完毕并关闭管道及文件指针*/
       res = pclose(fp);
       if ( res == -1 || pword=="")
       {
           //printf("close popen file pointer fp error!\n");
           return "";
       }
       else
       {
           //printf("popen res is :%d\n", res);
           return pword;

       }
}

void MainWindow::on_pushButton_clicked()
{
    QString xx=MainWindow::shell("xprop | grep 'CLASS' | awk -F '\"' '{print $4}'");
    QString uu = ui->lineEdit->text();
    qDebug()<<xx;
    string yy = MainWindow::turnstring(xx);
    string yy2 = MainWindow::turnstring(uu);
    vector<string> strs = split(yy,"\n");
    string zz="if [ -e $HOME/.devilspie/"+yy+".ds ] ; then echo 1; else echo 0; fi";
    cout<<zz<<endl;
    int i= std::atoi(zz.c_str());
    cout<<i<<endl;
    string gg = strs[0]+".ds";
    string zz2="0x"+yy2;
    string ff="echo \"(\n                if (contains (window_class) '"+strs[0]+"')\n                    (begin\n                        (spawn_async (str 'xprop -id ' (window_xid) ' -f _KDE_NET_WM_BLUR_BEHIND_REGION 32c -set _KDE_NET_WM_BLUR_BEHIND_REGION 0 '))\n                        (spawn_async (str 'xprop -id ' (window_xid) ' -f _NET_WM_WINDOW_OPACITY 32c -set _NET_WM_WINDOW_OPACITY "+zz2+" '))\n                    )\n             )\" > $HOME/.devilspie/"+gg;
    cout<<ff<<endl;
    QString final = QString::fromStdString(ff);
    MainWindow::shell(final);
    MainWindow::flash();
}

void MainWindow::on_pushButton_2_pressed()
{
    MainWindow::shell("x-terminal-emulator -e nohup devilspie &");
    cout<<1<<endl;
}
void MainWindow::remove(QString words)
{
    MainWindow::shell("rm $HOME/.devilspie/"+words);
    MainWindow::flash();
}
void MainWindow::edit(QString words)
{
    MainWindow::shell("xdg-open $HOME/.devilspie/"+words);
    MainWindow::flash();
}
void MainWindow::flash()
{
    signalMapper = new QSignalMapper(this);
    signalMapper2 = new QSignalMapper(this);
    QList<QLabel* > labs =ui->scrollAreaWidgetContents->findChildren<QLabel*>();
    QList<QPushButton*> btns = ui->scrollAreaWidgetContents->findChildren<QPushButton*>();
    ui->gridLayout->setColumnStretch(0,8);
    ui->gridLayout->setColumnStretch(1,4);
    ui->gridLayout->setColumnStretch(2,8);
    ui->gridLayout->setColumnStretch(2,8);
    ui->gridLayout->setColumnStretch(3,8);
    ui->gridLayout->setColumnStretch(4,8);
    ui->gridLayout->setColumnStretch(5,2);
    ui->gridLayout->setColumnStretch(6,2);
    foreach (QPushButton* btn, btns) {   delete btn;  }
    foreach (QLabel* lab, labs) { delete lab;}
    QString gg = MainWindow::shell("ls $HOME/.devilspie | awk -F '.ds' '{print $1}'");
    QString gg2 = MainWindow::shell("ls $HOME/.devilspie");
   // QString gg3 =MainWindow::shell("cat $HOME/.devilspie/glass.ds | grep '0x' | awk -F ' ' '{print $13}'");
    string ff = MainWindow::turnstring(gg);
    string ff2 =MainWindow::turnstring(gg2);
   // string ff3 =MainWindow::turnstring(gg3);
    vector<string> strs = split(ff,"\n");
    vector<string> strs2 = split(ff2,"\n");
    //vector<string> strs3 = split(ff3,"\n");
    QLabel *a=new QLabel;
    a->setText("名字");
    QLabel *b=new QLabel;
    b->setText("透明度");
    ui->gridLayout->addWidget(a,0,0);
    ui->gridLayout->addWidget(b,0,1);
    for (int i=0;i<strs.size();i++) {
        char yyy[100];
        char zzz[100];
        char xxx[100];
        string c ="cat $HOME/.devilspie/"+strs2[i]+" | grep '0x' | awk -F ' ' '{print $13}'";
        QString c2 =QString::fromStdString(c);
        QString gg3=MainWindow::shell(c2);
        string ff3 = MainWindow::turnstring(gg3);
        vector<string> strs3 = split(ff3,"\n");
        strcpy(yyy,strs[i].c_str());
        strcpy(zzz,strs2[i].c_str());
        strcpy(xxx,strs3[0].c_str());
        QLabel* test1=new QLabel;
        test1->setText(yyy);
        QLabel* text1=new QLabel;
        text1->setText(xxx);
        QPushButton* test2=new QPushButton(QWidget::tr("移除"));
        QPushButton* test3=new QPushButton(QWidget::tr("编辑"));
        ui->gridLayout->addWidget(test1,i+1,0);
        ui->gridLayout->addWidget(text1,i+1,1);
        ui->gridLayout->addWidget(test2,i+1,5);
        ui->gridLayout->addWidget(test3,i+1,6);
        connect(test2,SIGNAL(pressed()),signalMapper,SLOT(map()),Qt::UniqueConnection);
        signalMapper->setMapping(test2,zzz);
        connect(signalMapper,SIGNAL(mapped(QString)),this,SLOT(remove(QString)),Qt::UniqueConnection);
        connect(test3,SIGNAL(pressed()),signalMapper2,SLOT(map()),Qt::UniqueConnection);
        signalMapper2->setMapping(test3,zzz);
        connect(signalMapper2,SIGNAL(mapped(QString)),this,SLOT(edit(QString)),Qt::UniqueConnection);
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    MainWindow::shell("pkill devilspie");
}

void MainWindow::pan()
{
    //this->hide();

    //新建QSystemTrayIcon对象
    mSysTrayIcon = new QSystemTrayIcon(this);
    //新建托盘要显示的icon
    QIcon icon = QIcon(":/ico/res/hide_pass.png");
    //将icon设到QSystemTrayIcon对象中
    mSysTrayIcon->setIcon(icon);
    //当鼠标移动到托盘上的图标时，会显示此处设置的内容
    mSysTrayIcon->setToolTip(QObject::trUtf8("透明"));
    connect(mSysTrayIcon,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this,SLOT(on_activatedSysTrayIcon(QSystemTrayIcon::ActivationReason)));
    //在系统托盘显示此对象
    cout<<QSystemTrayIcon::ActivationReason()<<endl;
    createActions();
    createMenu();
    mSysTrayIcon->show();
}
void MainWindow::on_activatedSysTrayIcon(QSystemTrayIcon::ActivationReason reason)
{
    cout<<reason<<endl;
    if (reason==3)
    {
        cout<<1<<endl;
        this->show();
    }
}
